# DBS - Internetový obchod so športovými potebami (backend)

Live verzia: https://dbsapi.chaoss.me/

### Authors
* **Lukáš Minár** - [AIS](https://is.stuba.sk/auth/lide/clovek.pl?id=96976;lang=sk), [GitHub](https://github.com/chaossme/), [Gitlab](https://gitlab.com/chaossme)
* **Michal Paulovič** - [AIS](https://is.stuba.sk/auth/lide/clovek.pl?id=97001;lang=sk), [GitHub](https://github.com/paulotheblack), [Gitlab](https://gitlab.com/paulotheblack/)
