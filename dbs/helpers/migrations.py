import os
from dbs.helpers.database import load_sql


def migrate(filename):
    file_path = os.path.join(os.path.dirname(__file__), '../migrations/sql/', os.path.basename(filename)[:-3] + ".sql")
    load_sql(file_path)