import os
from django.db import connection


def load_sql(file_path):
    sql_statement = open(file_path).read()
    with connection.cursor() as c:
        c.execute(sql_statement)