class QueryBuilder:
    _table = None
    _columns = ["*"]
    _where = []

    def where(self, col, operator, value):
        self._where.append((col, operator, value))

    def table(self, table):
        self._table = table

    def columns(self, columns):
        self._columns = columns

    def get(self):
        return f'''
            SELECT {",".join(self._columns)}
            FROM {self._table}
        '''
