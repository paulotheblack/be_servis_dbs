"""dbs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from dbs.views import Manufacturers, Products, Home, Orders, Categories, Dashboard, Collections, Report

urlpatterns = [
    path("", Home.index, name="index"),
    path("admin", admin.site.urls),
    path("dashboard", Dashboard.index, name="dashboard_index"),
    path("categories", Categories.index, name="categories_index"),
    path("collections/<str:collection_name>", Collections.index, name="collections_index"),
    path("products", Products.index, name="products_index"),
    path("products/<int:product_id>", Products.show, name="products_show"),
    path("orders", Orders.index, name="orders_index"),
    path("orders/<uuid:order_uuid>", Orders.show, name="orders_show"),
    path("manufacturers", Manufacturers.index, name="manufacturers_index"),
    path("manufacturers/<int:manufacturer_id>", Manufacturers.show, name="manufacturers_show"),
    path("orders/<uuid:order_uuid>", Orders.show, name="orders_show"),
    path("orders", Orders.index, name="orders_index"),
    path("orders/create", Orders.create, name="orders_create"),
    path("orders/update/<int:order_id>", Orders.update, name="orders_update"),
    path("orders/delete/<int:order_id>", Orders.delete, name="orders_delete"),
    path("report/<str:day>", Report.report, name="report_report")
]
