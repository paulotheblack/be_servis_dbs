from django.db import migrations
from dbs.helpers.migrations import migrate


class Migration(migrations.Migration):
    dependencies = [
        ("dbs", "003_create_product_parameter_table")
    ]
    operations = [
        migrations.RunPython(lambda apps, schema_editor: migrate(__file__)),
    ]