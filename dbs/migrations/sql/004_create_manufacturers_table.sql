CREATE TABLE manufacturers (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE,
    img_urls text
);
ALTER TABLE products ADD COLUMN manufacturer_id INT;
ALTER TABLE products
ADD CONSTRAINT fk_manufacturer_id FOREIGN KEY (manufacturer_id)
REFERENCES manufacturers (id)
ON UPDATE CASCADE ON DELETE CASCADE;