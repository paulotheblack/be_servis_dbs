CREATE TABLE products (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE,
    price decimal(6,2) NOT NULL,
    description text,
    img_urls text
);