CREATE TABLE categories (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    description text,
    parent_id INT,
    CONSTRAINT fk_categories_categories FOREIGN KEY (parent_id)
    REFERENCES categories (id)
    ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE products ADD COLUMN category_id INT;
ALTER TABLE products
ADD CONSTRAINT fk_categories_id FOREIGN KEY (category_id)
REFERENCES categories (id)
ON UPDATE CASCADE ON DELETE CASCADE;
