CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    uuid uuid UNIQUE,
    date timestamp,
    price decimal(8,2) NOT NULL,
    status smallint NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone_number varchar(255) NOT NULL,
    address varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    zip varchar(20) NOT NULL,
    country varchar(255) NOT NULL
);

CREATE INDEX uuid_index ON orders(uuid);