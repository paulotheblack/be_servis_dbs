CREATE TABLE parameters (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE
);