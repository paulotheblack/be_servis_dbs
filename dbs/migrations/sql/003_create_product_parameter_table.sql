CREATE TABLE product_parameter (
    product_id int NOT NULL,
    parameter_id int NOT NULL,
    value varchar(255) NOT NULL,
    CONSTRAINT fk_products_parameters
        FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_parameters_products
        FOREIGN KEY (parameter_id)
        REFERENCES parameters (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)