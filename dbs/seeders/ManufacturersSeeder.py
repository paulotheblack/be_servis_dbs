from django.db import connection
from faker import Faker
from random import *
import time

class ManufacturersSeeder:
    @staticmethod
    def run(count=100000, delete=True):
        with connection.cursor() as cursor:
            if delete:
                print("deleting...")
                cursor.execute("DELETE FROM manufacturers")

            manufacturers = ['NULL']
            fake = Faker("en_US")

            print(f"Seeding {count} manufacturers...")
            for i in range(count):
                name = " ".join(fake.words(nb=randint(3, 5), unique=True))
                img_urls = f"https://picsum.photos/800/450?random&t={time.time()}"
                cursor.execute(f'''
                    INSERT INTO manufacturers
                        ("name", "img_urls")
                    VALUES
                        ('{name}', '{img_urls}')
                    RETURNING id;
                ''')
                manufacturers_id = cursor.fetchone()[0]