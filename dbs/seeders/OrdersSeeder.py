from django.db import connection
from faker import Faker
from random import *
import datetime
import uuid

class OrdersSeeder:
    @staticmethod
    def run(count=1000000000, delete=False):
        print(f"Seeding {count} orders...")
        with connection.cursor() as cursor:
            if delete:
                print("deleting...")
                cursor.execute("DELETE FROM orders")
            fake = Faker("sk_SK")

            # insert 1M+1 orders
            for i in range(count):
                cursor.execute(f"SELECT id, price FROM products ORDER BY random() LIMIT {randint(1, 20)}")
                random_products = cursor.fetchall()
                date = fake.date_time_between_dates(datetime.datetime(2017, 1, 1), datetime.datetime.today())
                total_price = 0
                status = randint(0, 3) # 0 = not ready, 1 = ready, 2 = shipping, 3 = delivered
                first_name = fake.first_name().replace("'", "''")
                last_name = fake.last_name().replace("'", "''")
                email = fake.email()
                phone_number = fake.phone_number()
                address = fake.address().replace("'", "''")
                city = fake.city().replace("'", "''")
                zip = fake.postcode()
                country = fake.country().replace("'", "''")
                _uuid = uuid.uuid4()
                for id, price in random_products:
                    total_price += price
                total_price = float("{0:.2f}".format(total_price))

                cursor.execute(f'''
                    INSERT INTO orders
                        ("date", "price", "status", "first_name", "last_name", "email", "phone_number", "address", "city", "zip", "country", "uuid")
                    VALUES
                        ('{date}','{total_price}','{status}','{first_name}','{last_name}','{email}','{phone_number}','{address}','{city}','{zip}','{country}', '{_uuid}')
                    RETURNING id;
                ''')

                order_id = cursor.fetchone()[0]

                for product_id, price in random_products:
                    cursor.execute(f'''
                        INSERT INTO order_product
                            ("order_id", "product_id", "quantity")
                        VALUES
                            ({order_id}, {product_id}, {randint(1,6)})
                    ''')