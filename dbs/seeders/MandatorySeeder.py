from random import randint

from django.db import connection
from faker import Faker

class MandatorySeeder:

    @classmethod
    def insert_batch(cls, batch, cursor):
        cursor.execute(f"""
            INSERT INTO parameters
            ("name")
            VALUES
            {",".join(map(lambda i: f"('{i}')", batch))}
        """)

    @staticmethod
    def run(count=100000, delete=True):
        with connection.cursor() as cursor:
            fake = Faker("en_US")

            print(f"Seeding {count} random parameters...")
            batch = []
            for i in range(count):
                batch.append(" ".join(fake.words(nb=randint(1, 5))))
                if i % 10 == 0:
                    MandatorySeeder.insert_batch(batch, cursor)
                    batch = []
            MandatorySeeder.insert_batch(batch, cursor)
