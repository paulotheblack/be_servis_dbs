from django.db import connection
from faker import Faker
from random import *
from dbs.models_django.Model import Product, Parameter


class ParametersSeeder:
    @staticmethod
    def run(count=100000, delete=False):
        with connection.cursor() as cursor:
            if delete:
                print("deleting parameters...")
                cursor.execute("DELETE FROM parameters")
                cursor.execute("DELETE FROM product_parameter")
            fake = Faker("sk_SK")

            print(f"Seeding {count} parameters...")
            parameters = [
                [None, "Farba", "straighten", lambda: fake.color()],
                [None, "Overené PETA", "pets", lambda: choice(["Áno", "Nie"])],
                [None, "IP krytie", "opacity", lambda: f"{choice(['X', randint(0, 6)])}{choice(['X', randint(0, 9)])}{choice(['C', 'D', 'f', 'H', 'M', 'S', 'W'])}"],
                [None, "Materiál", "lens", lambda: choice([
                    "Drevo (dub)",
                    "Drevo (buk)",
                    "Hliník",
                    "Kov",
                ])],
                [None, "Veľkosť (cm)", "color_lens", lambda: randint(10, 500)],
                [None, "Hmotnosť (g)", "confirmation_number", lambda: randint(100, 10000)],
                [None, "Životnosť batérie (rok)", "battery_alert", lambda: randint(1, 20)],
                [None, "Dosah bluetooth (m)", "settings_remote", lambda: randint(5, 20)],
                [None, "Operačný systém", "android", lambda: choice([
                    "Android",
                    "Windows",
                    "Debian",
                    "Ubuntu",
                    "Symbian",
                    "iOS"
                ])],

            ]

            # insert parameters
            for i in range(len(parameters)):
                name = parameters[i][1]
                icon = parameters[i][2]
                cursor.execute(f'''
                    INSERT INTO parameters
                        ("name", "icon")
                    VALUES
                        ('{name}', '{icon}')
                    RETURNING id;
                ''')
                parameters[i][0] = cursor.fetchone()[0]

            # assign each product parameters
            for product in Product.objects.all():
                _parameters = sample(parameters, randint(2, len(parameters)))
                for parameter in _parameters:
                    cursor.execute(f'''
                        INSERT INTO product_parameter
                            ("product_id", "parameter_id", "value")
                        VALUES
                            ({product.id}, {parameter[0]}, '{parameter[3]()}')
                    ''')
