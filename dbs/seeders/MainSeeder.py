from dbs.seeders.ProductSeeder import ProductSeeder
from dbs.seeders.ParametersSeeder import ParametersSeeder
from dbs.seeders.CategoriesSeeder import CategoriesSeeder
from dbs.seeders.ManufacturersSeeder import ManufacturersSeeder
from dbs.seeders.OrdersSeeder import OrdersSeeder


class MainSeeder:
    @staticmethod
    def run(count, delete):
        ParametersSeeder.run(count, delete)
        CategoriesSeeder.run(count, delete)
        ManufacturersSeeder.run(count, delete)
        ProductSeeder.run(count, delete)
        OrdersSeeder.run(count, delete)

