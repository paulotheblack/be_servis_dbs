from django.db import connection
from faker import Faker
from random import *
import time


class ProductSeeder:
    @staticmethod
    def run(count=100000, delete=False):
        with connection.cursor() as cursor:
            if delete:
                print("deleting...")
                cursor.execute("DELETE FROM product_parameter")
                cursor.execute("DELETE FROM products")

            # get all parameters
            cursor.execute("SELECT * FROM parameters")
            parameters = cursor.fetchall()

            # get manufacturers_id
            manufacturers = ['NULL']
            cursor.execute("SELECT id FROM manufacturers")
            manufacturers = cursor.fetchall()

            categories = ['NULL']
            cursor.execute("SELECT id FROM categories")
            categories = cursor.fetchall()

            fake = Faker("sk_SK")

            print(f"Seeding {count} products...")
            for i in range(count):
                name = choice(["Bicykel", "Elektrobicykel", "Kolobezka", "Elektrokolobezka",
                                "Surf", "Paddleboard", "Cln", "Kajak", "Kolieskove korcule",
                                "Ladove korcule", "Skateboard", "Longboard", "Pennyboard",
                                "Waveboard", "Elektricky longboard", "Skakacie topanky",
                                "Trampolina", "Badmintonova raketa", "Tenisova raketa", "Squashova raket"
                               ]) + " " + " ".join(fake.words(nb=randint(3, 7), unique=True))
                price = float("{0:.2f}".format(uniform(1, 1000)))
                description = fake.paragraph(randint(1, 10))
                img_urls = ",".join([f"https://picsum.photos/400/800?random&t={time.time()}" for _ in range(randint(2, 8))])
                manufacturer_id = choice(manufacturers)[0]
                category_id = choice(categories)[0]
                cursor.execute(f'''
                    INSERT INTO products
                        ("name", "price", "description", "img_urls", "manufacturer_id", "category_id")
                    VALUES
                        ('{name}', {price}, '{description}', '{img_urls}', {manufacturer_id}, {category_id})
                    RETURNING id;
                ''')

