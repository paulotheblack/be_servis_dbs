from django.db import connection
from faker import Faker
from random import *


class CategoriesSeeder:
    @staticmethod
    def run(count=100000, delete=False):
        with connection.cursor() as cursor:
            # DELETE all existing
            if delete:
                print("deleting...")
                cursor.execute("DELETE FROM categories")
            categories = ['NULL']
            fake = Faker("en_US")

            print(f"Seeding {count} categories...")
            for i in range(count):
                name = " ".join(fake.words(1, unique=True))
                description = fake.paragraph(randint(20, 100))
                parent_id = choice(categories)
                cursor.execute(f'''
                    INSERT INTO categories
                        ("name", "description", "parent_id")
                    VALUES
                        ('{name}', '{description}', {parent_id})
                    RETURNING id;
                ''')
                categories_id = cursor.fetchone()[0]
                categories.append(categories_id)