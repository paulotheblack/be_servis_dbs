from django.core.management.base import BaseCommand, CommandError
from dbs.models.Reporter import Reporter


class Command(BaseCommand):
    def handle(self, *args, **options):
        Reporter.send()
