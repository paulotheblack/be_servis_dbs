from django.core.management.base import BaseCommand, CommandError
from dbs.seeders.CategoriesSeeder import CategoriesSeeder
from dbs.seeders.MainSeeder import CategoriesSeeder
from dbs.seeders.ManufacturersSeeder import ManufacturersSeeder
from dbs.seeders.OrdersSeeder import OrdersSeeder
from dbs.seeders.ParametersSeeder import ParametersSeeder
from dbs.seeders.ProductSeeder import ProductSeeder
from dbs.seeders.MandatorySeeder import MandatorySeeder


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--class',
            type=str,
            help='Specify seeder',
        )
        parser.add_argument(
            '--count',
            type=int,
            help='Specify how many items should be seeded.',
        )
        parser.add_argument(
            '-d',
            '--delete',
            action="store_false",
            help='Table will be truncated.',
        )

    def handle(self, *args, **options):
        class_name = options['class'] or "MainSeeder"
        count = options['count'] or 10000
        delete = not options['delete']
        eval(class_name).run(count, delete)
