from django.db import models


class Manufacturer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)
    img_urls = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'manufacturers'


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    parent = models.ForeignKey(
        'self', blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'categories'


class Parameter(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)

    class Meta:
        db_table = 'parameters'


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    description = models.TextField(blank=True, null=True)
    img_urls = models.TextField(blank=True, null=True)
    visited = models.PositiveIntegerField(blank=True, null=True)
    manufacturer = models.ForeignKey(
        'Manufacturer', blank=True, null=True, on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        'Category', blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'products'


class ProductParameter(models.Model):
    product = models.ForeignKey(
        'Product', on_delete=models.CASCADE
    )
    parameter = models.ForeignKey(
        'Parameter', on_delete=models.CASCADE
    )
    value = models.CharField(max_length=255)

    class Meta:
        db_table = 'product_parameter'


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(unique=True, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    status = models.SmallIntegerField()
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    zip = models.CharField(max_length=20)
    country = models.CharField(max_length=255)

    class Meta:
        db_table = 'orders'


class OrderProduct(models.Model):
    order = models.ForeignKey(
        'Order', on_delete=models.CASCADE
    )
    product = models.ForeignKey(
        'Product', primary_key=True, on_delete=models.CASCADE
    )
    quantity = models.IntegerField()

    class Meta:
        db_table = 'order_product'