import datetime
from django.db import connection

from dbs.models.Category import Category
from dbs.models.Manufacturer import Manufacturer
from dbs.models.Order import Order
from django.http import JsonResponse

from dbs.models.Product import Product


def index(request):
    with connection.cursor() as cursor:
        orders_newest_count, orders_newest = Order.get(cursor, 10, 0, ("date", True))
        orders_newest_count, orders_priority = Order.get(cursor, 10, 0, ("status", False))

        return JsonResponse({
            "status": 200,
            "data": {
                "products_count": Product.count_all(cursor),
                "manufacturers_count": Manufacturer.count_all(cursor),
                "categories_count": Category.count_all(cursor),
                "orders_count": Order.count_all(cursor),
                "orders_newest": orders_newest,
                "orders_priority": orders_priority,
            }
        }, safe=False)
