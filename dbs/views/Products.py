from math import ceil, floor

from django.http import JsonResponse, HttpResponseNotFound
from django.db import connection


# ----------------- #
#       _index      #
# ----------------- #
from dbs.models.Category import Category
from dbs.models.Model import Model
from dbs.models.Product import Product


def index(request):
    products = []
    with connection.cursor() as cursor:
        # parse query @par: min_price, max_price, category_id, manufacturer_id
        # url example: /products/?min_price=10&max_price=5000&category=10
        wquery = ""
        order_by_query = ""
        meta = {}
        has_where = 0

        limit = int(request.GET.get("limit", 5))
        offset = int(request.GET.get("offset", 0))
        sort = request.GET.get("sort", "")

        category = None
        category_id = request.GET.get("category", None)
        if category_id:
            cursor.execute(f"SELECT * FROM categories WHERE id={category_id}")
            category = cursor.fetchone()

            # if no such category was found, return 404
            if not category:
                return HttpResponseNotFound()

        breadcrumb = []
        if category:
            # TODO for lukas, etapa neskorsia: scoped
            wquery += f" {'AND' if has_where else 'WHERE'} category_id={category_id}"
            has_where = 1

            category_id_temp = category_id
            while category_id_temp is not None:
                cursor.execute(f"SELECT id, name, parent_id FROM categories WHERE id={category_id_temp}")
                parent = cursor.fetchone()
                breadcrumb.append(parent)
                category_id_temp = parent[-1] if parent else None
            breadcrumb = list(map(lambda item: {"id": item[0], "label": item[1]}, reversed(breadcrumb)))

        order_by_popularity = False
        if sort:
            if sort == "popular":
                order_by_popularity = True
                order_by_query = "ORDER BY count DESC"
            elif sort == "name":
                order_by_query = "ORDER BY P.name"
            elif sort == "name_desc":
                order_by_query = "ORDER BY P.name DESC"
            elif sort == "price":
                order_by_query = "ORDER BY price"
            elif sort == "price_desc":
                order_by_query = "ORDER BY price DESC"

        cursor.execute(f"SELECT id, name FROM manufacturers WHERE id IN (select manufacturer_id from products {wquery})")
        _manufacturers = cursor.fetchall()

        manufacturer = request.GET.get("manufacturer", None)
        if manufacturer:
            wquery += f" {'AND' if has_where else 'WHERE'} manufacturer_id={manufacturer}"
            has_where = 1

        price_wquery = f""

        min_price = request.GET.get("min_price", None)
        if min_price:
            price_wquery += f"WHERE price >= {min_price}"

        max_price = request.GET.get("max_price", None)
        if max_price:
            price_wquery += f" {'AND' if max_price else 'WHERE'} price <= {max_price}"

        manufacturer_cols =["id", "name"]
        product_cols =["id", "name", "price", "description", "img_urls"]
        all_cols = ["count"] + product_cols + [":manufacturer"] + manufacturer_cols

        if order_by_popularity:
            cursor.execute(f'''
                        SELECT
                            _.*,
                            COUNT(*) OVER() AS full_count
                        FROM (
                            SELECT
                                C.count,
                                P.{",P.".join(product_cols)},
                                ':manufacturer', M.{",M.".join(manufacturer_cols)},
                                MIN(P.price) OVER(),
                                MAX(P.price) OVER()
                            FROM products P

                            INNER JOIN manufacturers M
                            ON M.id=P.manufacturer_id

                            INNER JOIN (
                                SELECT COUNT(product_id), product_id FROM order_product op
                                GROUP BY op.product_id
                                HAVING COUNT(product_id) > 0
                            ) C ON P.id=C.product_id

                            {wquery}

                            {order_by_query}
                        ) _

                        {price_wquery}

                        LIMIT {limit}
                        OFFSET {offset * limit}
                    ''')

        else:
            cursor.execute(f'''
                SELECT
                    _.*,
                    COUNT(*) OVER() AS full_count
                FROM (
                    SELECT
                        P.{",P.".join(product_cols)},
                        ':manufacturer', M.{",M.".join(manufacturer_cols)},
                        MIN(P.price) OVER(),
                        MAX(P.price) OVER()
                    FROM products P

                    INNER JOIN manufacturers M
                    ON M.id=P.manufacturer_id

                    {wquery}

                    {order_by_query}
                ) _

                {price_wquery}

                LIMIT {limit}
                OFFSET {offset * limit}
            ''')

        products_data = cursor.fetchall()

        _min_price = 0
        _max_price = 0
        if len(products_data) > 0:
            meta["full_count"] = products_data[-1][-1]
            _min_price = floor(products_data[0][-3])
            _max_price = ceil(products_data[0][-2])

        products = Model.serializeJoin(all_cols if order_by_popularity else all_cols[1:], products_data)

        # include children categories
        children_categories = Category.children(category_id, cursor, ["id", "name"])

        return JsonResponse({
            "status": 200,
            "meta": meta,
            "data": {
                "min_price": _min_price if _min_price else 0,
                "max_price": _max_price if _max_price else 0,
                "products": products,
                "breadcrumb": breadcrumb,
                "children_categories": children_categories,
                "manufacturers": list(map(lambda item: {"id": item[0], "name": item[1]}, _manufacturers)),
            }
        }, safe=False)


def show(request, product_id):
    with connection.cursor() as cursor:
        product = Product.find(product_id, cursor)

        # return 404 if product was found
        if not product:
            return HttpResponseNotFound()

        # increment visited to be able to sort by popular
        Product.increment_visited(product_id, cursor)

        # include relationships
        product["manufacturer"] = Product.manufacturer(product["id"], cursor)
        product["category"] = Product.category(product["id"], cursor)

        cursor.execute(f'''
            SELECT
                   P.name, P.icon, PP.value
            FROM product_parameter PP
            
            INNER JOIN parameters P
            ON P.id = PP.parameter_id

            WHERE product_id={product_id};
        ''')
        parameters = list(map(lambda item: {"name": item[0], "icon": item[1], "value": item[2]}, cursor.fetchall()))
        product["parameters"] = parameters

        return JsonResponse({
            "status": 200,
            "data": product
        }, safe=False)
