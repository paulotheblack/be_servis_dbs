from django.http import JsonResponse
from django.db import connection
from dbs.models.Product import Product


def index(request, collection_name):
    with connection.cursor() as cursor:
        limit = int(request.GET.get("limit", 20))
        offset = int(request.GET.get("offset", 0))
        products = []
        full_count = 0

        if collection_name == "popular":
            full_count, products = Product.get_popular(limit, offset, cursor)
        if collection_name == "top-battery":
            full_count, products = Product.get_with_above_parameter_value("Životnosť batérie (rok)", limit, offset, cursor)

        return JsonResponse({
            "meta": {
              "full_count": full_count
            },
            "status": 200,
            "data": products,
        })