from django.db import connection
from dbs.models.Order import Order
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from datetime import datetime
from dbs.models.Order import Order as OurOrder
from dbs.models_django.Model import Product, Order, OrderProduct
import uuid
import json
from django.http import HttpResponseBadRequest


def index(request):
    with connection.cursor() as cursor:
        search = request.GET.get("search", None)
        limit = int(request.GET.get("limit", 5))
        offset = int(request.GET.get("offset", 0))

        meta = {}
        if search:
            search_str = search.replace("+", " ")
            full_count, orders = OurOrder.search(cursor, search_str, limit, offset)
            meta["full_count"] = full_count
        else:
            full_count, orders = OurOrder.get(cursor, limit, offset)
            meta["full_count"] = full_count

        return JsonResponse({
            "status": 200,
            "meta": meta,
            "data": orders
        }, safe=False)


def show(request, order_uuid):
    with connection.cursor() as cursor:
        order = OurOrder.find_by_uuid(order_uuid, cursor)

        # return 404 if order was not found
        if not order:
            return HttpResponseNotFound()

        order["products"] = OurOrder.products(order_uuid, cursor)
        return JsonResponse({
            "status": 200,
            "data": order
        }, safe=False)

@csrf_exempt
def update(request, order_id):
    if request.method == "PATCH":
        Order.objects.filter(id=order_id).update(**json.loads(request.body))
        return HttpResponse(status=200)
    else:
        return HttpResponseBadRequest()

@csrf_exempt
def delete(request, order_id):
    if request.method == "DELETE":
        Order.objects.filter(id=order_id).delete()
        return HttpResponse(status=200)
    else:
        return HttpResponseBadRequest()


def date_converter(date):
    if isinstance(date, datetime.datetime):
        return date.__str__()


@csrf_exempt
def create(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        products_data = json_data["products"].split(",")
        products = [{"id": int(product.split(".")[0]), "quantity": int(product.split(".")[1])} for product in products_data]
        productid_quantity = list(map(lambda item: (item["id"], item["quantity"]), products))
        productid_quantity_string = ", ".join(list(map(lambda item: f"({item[0]}, {item[1]})", productid_quantity)))

        with connection.cursor() as cursor:
            cursor.execute(f'''
                SELECT
                    SUM(_.product_price) AS total_price
                FROM
                (
                    SELECT
                           P.price * V.quantity as product_price
                    FROM products AS P
                
                    RIGHT JOIN (
                        VALUES {productid_quantity_string}
                    ) as V(id, quantity)
                    ON P.id=V.id
                ) _
            ''')
            total_price = float(cursor.fetchone()[0])
            _uuid = uuid.uuid4()

            order = Order(
                uuid=_uuid,
                date=datetime.now(tz=timezone.utc),
                price=total_price,
                status=0,
                first_name=json_data["first_name"],
                last_name=json_data["last_name"],
                email=json_data["email"],
                phone_number=json_data["phone_number"],
                address=json_data["address"],
                city=json_data["city"],
                zip=json_data["zip"],
                country=json_data["country"]
            )
            order.save()

            for product in products:
                op = OrderProduct(
                    order_id=order.id,
                    product_id=product["id"],
                    quantity=product["quantity"]
                )
                op.save(force_insert=True, force_update=False)

            return JsonResponse({
                "status": 200,
                "data": {
                    "uuid": _uuid
                }
            }, safe=False)
