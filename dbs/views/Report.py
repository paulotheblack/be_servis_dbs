from django.http import HttpResponse
from dbs.models.Reporter import Reporter


def report(request, day):
    response, pdf = Reporter.get_report(day)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)
