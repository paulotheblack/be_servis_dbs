from django.http import JsonResponse
from django.db import connection
from dbs.models.Category import Category


def index(request):
    with connection.cursor() as cursor:
        search = request.GET.get("search", None)

        if search:
            search_str = search.replace("+", " ")
            categories = Category.search(cursor, search_str)
        else:
            categories = Category.get(cursor)

        return JsonResponse({
            "status": 200,
            "data": categories
        }, safe=False)