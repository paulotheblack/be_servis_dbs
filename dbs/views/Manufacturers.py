from django.http import JsonResponse, HttpResponseNotFound
from django.db import connection
from dbs.models.Manufacturer import Manufacturer


def index(request):
    with connection.cursor() as cursor:
        search = request.GET.get("search", None)
        limit = int(request.GET.get("limit", 5))
        offset = int(request.GET.get("offset", 0))

        meta = {}
        if search:
            search_str = search.replace("+", " ")
            full_count, manufacturers = Manufacturer.search(cursor, search_str, limit, offset)
            meta["full_count"] = full_count
        else:
            full_count, manufacturers = Manufacturer.get(cursor, limit, offset)
            meta["full_count"] = full_count

        return JsonResponse({
            "status": 200,
            "meta": meta,
            "data": manufacturers
        }, safe=False)


def show(request, manufacturer_id):
    with connection.cursor() as cursor:
        manufacturer = Manufacturer.find(manufacturer_id, cursor)

        # return 404 if manufacturer was not found
        if not manufacturer:
            return HttpResponseNotFound()

        # include most popular products
        manufacturer["products"] = Manufacturer.most_popular_products(manufacturer_id, cursor, limit=5)

        # include the manufacturer product count
        manufacturer["products_count"] = Manufacturer.products_count(manufacturer_id, cursor)

        return JsonResponse({
            "status": 200,
            "data": manufacturer
        }, safe=False)
