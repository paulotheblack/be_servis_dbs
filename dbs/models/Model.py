from django.db import connection
from dbs.helpers.QueryBuilder import QueryBuilder


class Model:
    table = None
    _columns = ["*"]

    @classmethod
    def serialize(cls, item, cols):
        _item = {}
        for i in range(len(cols)):
            col = cols[i]
            if col == "img_urls":
                _item[col] = item[i].split(",")
            else:
                _item[col] = item[i]
        return _item

    @classmethod
    def columns(cls, prefix=""):
        return list(map(lambda column_name: prefix + column_name, cls._columns))

    @classmethod
    def get(cls, cursor, limit, offset, order_by=None):
        order_by_str = ""
        if order_by:
            order_by_str = f'ORDER BY {order_by[0]} {"DESC" if order_by[1] else ""}'
        cursor.execute(f'''
                SELECT {",".join(cls._columns)},
                COUNT(*) OVER() AS full_count
                FROM {cls.table}
                {order_by_str if order_by else ""}
                LIMIT {limit}
                OFFSET {limit*offset}
            ''')

        _items = cursor.fetchall()
        items = [cls.serialize(item[:-1], cls._columns) for item in _items]

        if len(items) > 0:
            full_count = _items[0][-1]
            return full_count, items
        else:
            return 0, []

    @classmethod
    def find(cls, id, cursor):
        cursor.execute(f'''
                SELECT {",".join(cls._columns)}
                FROM {cls.table}
                WHERE id={id}
            ''')
        _item = cursor.fetchone()

        # if found, serialize it
        return cls.serialize(_item, cls._columns) if _item else None

    @classmethod
    def children(cls, id, cursor, columns=None):

        if not columns:
            columns = cls._columns

        cursor.execute(f'''
            SELECT {",".join(columns)}
            FROM {cls.table}
            WHERE parent_id{f"={id}" if id else ' IS NULL'}
            LIMIT 10;
        ''')

        children = cursor.fetchall()
        children = [cls.serialize(child, columns) for child in children]
        return children

    @classmethod
    def serializeItem(cls, key, value):
        if key == "img_urls":
            return value.split(",")
        if key == "price":
            return float(value)
        return value

    @classmethod
    def serializeJoin(cls, columns, result):
        items = []
        for res in result:
            obj = {}
            curr = None
            for i in range(len(columns)):
                key = columns[i]
                value = res[i]
                if key[0] == ":":
                    curr = key[1:]
                    obj[curr] = {}
                    continue

                if curr is None:
                    obj[key] = Model.serializeItem(key, value)
                else:
                    obj[curr][key] = Model.serializeItem(key, value)
            items.append(obj)
        return items

    @classmethod
    def search(cls, cursor, str, limit=5, offset=0):
        cursor.execute(f'''
            SELECT
                {",".join(cls._columns)},
                COUNT(*) OVER() AS full_count
            FROM {cls.table}
            WHERE name LIKE '%{str}%'
            LIMIT {limit}
            OFFSET {limit*offset}
        ''')
        _items = cursor.fetchall()
        items = [cls.serialize(item[:-1], cls._columns) for item in _items]
        if len(items) > 0:
            full_count = _items[0][-1]
            return full_count, items
        else:
            return 0, []
    @classmethod
    def count_all(cls, cursor):
        cursor.execute(f'''
            SELECT COUNT(*) FROM {cls.table}
        ''')

        count = cursor.fetchone()[0]
        return count
