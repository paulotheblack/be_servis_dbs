import datetime
from io import BytesIO
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from dbs.models_django.Model import Order
from dbs.settings import BASE_DIR
from django.core.mail import EmailMessage


class Reporter:
    @classmethod
    def send(cls):
        today = datetime.date.today().strftime('%Y%m%d')
        response, pdf = Reporter.get_report(today)
        filename = f"{BASE_DIR}/dbs/storage/{today}.pdf"

        with open(filename, 'wb+') as output:
            output.write(response.getbuffer())

        nice_today = datetime.date.today().strftime('%d. %m. %Y')
        message = EmailMessage(
            f"Zoznam objednavok za den {nice_today}",
            f'V prilohe je zoznam objednavok za den {nice_today}',
            'dbs.admin@chaoss.me',
            ['dbs.admin@chaoss.me'],
        )

        message.attach_file(filename)
        message.send()

    @classmethod
    def get_report(cls, day):
        template = get_template("../templates/order.html")

        date = datetime.datetime.strptime(day, '%Y%m%d')
        start = date
        end = date + datetime.timedelta(days=1)
        orders = Order.objects.filter(
            date__gte=start, date__lte=end
        ).order_by("date")[:50]

        html = template.render({"orders": orders, "date": date})
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), response)
        return response, pdf
