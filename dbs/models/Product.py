from dbs.models.Category import Category
from dbs.models.Manufacturer import Manufacturer
from dbs.models.Model import Model


class Product(Model):
    table = "products"
    _columns = ["id", "name", "price", "description", "img_urls", "visited"]

    @classmethod
    def manufacturer(cls, product_id, cursor):
        cols = Manufacturer.columns(prefix="m.")
        cursor.execute(f'''
            SELECT {",".join(cols)} FROM manufacturers m
            INNER JOIN products p
            ON p.manufacturer_id=m.id
            WHERE p.id={product_id}
        ''')
        manufacturer = cursor.fetchone()
        manufacturer = cls.serialize(manufacturer, Manufacturer.columns())
        return manufacturer

    @classmethod
    def category(cls, product_id, cursor):
        cols = Category.columns(prefix="c.")
        cursor.execute(f'''
                SELECT {",".join(cols)} FROM categories c
                INNER JOIN products p
                ON p.category_id=c.id
                WHERE p.id={product_id}
            ''')
        category = cursor.fetchone()
        category = cls.serialize(category, Category.columns())
        return category

    @classmethod
    def increment_visited(cls, product_id, cursor):
        cursor.execute(f'''
            UPDATE products
            SET visited = visited + 1
            WHERE id={product_id};
        ''')

    @classmethod
    def get_popular(cls, limit, offset, cursor):
        cursor.execute(f'''
                    SELECT
                        P.{",P.".join(Product._columns)},
                        ':category', C.{",C.".join(Category.columns())},
                        ':manufacturer', M.{",M.".join(Manufacturer.columns())},
                        COUNT(*) OVER() AS full_count
                    FROM products P

                    INNER JOIN manufacturers M
                    ON P.manufacturer_id=M.id

                    INNER JOIN categories C
                    ON P.category_id=C.id

                    WHERE visited > 0

                    ORDER BY P.visited DESC
                    LIMIT {limit}
                    OFFSET {offset * limit}
                ''')

        result = cursor.fetchall()
        full_count = 0
        if len(result) > 0:
            full_count = result[0][-1]
        cols = Product.columns() + [":category"] + Category.columns() + [":manufacturer"] + Manufacturer.columns()
        products = Model.serializeJoin(cols, result)

        return full_count, products

    @classmethod
    def get_with_above_parameter_value(cls, param_name, limit, offset, cursor):
        cursor.execute(f'''
            SELECT
                P.{",P.".join(Product._columns)},
                t.durability as durability,
                RANK() OVER(PARTITION BY category_id ORDER BY t.durability DESC) as c_rank,
                ':category', C.{",C.".join(Category.columns())},
                ':manufacturer', M.{",M.".join(Manufacturer.columns())},
                COUNT(*) OVER() AS full_count
            -- Get only products, which contain given parameter, and cast its value to float
            FROM (
                SELECT
                    product_id,
                    CAST(value as FLOAT) as durability
                FROM product_parameter

                INNER JOIN parameters PARAM
                ON PARAM.id=parameter_id

                WHERE VALUE ~ '^[+-]?([0-9]*[.])?[0-9]+$'
                AND PARAM.name='{param_name}'
            ) t
            
            INNER JOIN products P
            ON P.id=t.product_id
            
            INNER JOIN manufacturers M
            ON P.manufacturer_id=M.id
                        
            INNER JOIN categories C
            ON P.category_id=C.id

            -- filter only products whose given parameter's value is greater than average
            WHERE durability > (
                -- Get average value of that parameter
                SELECT
                    AVG(VALUE)
                FROM (
                    SELECT
                        CAST(PP.value AS FLOAT)
                    FROM parameters PARAM
            
                    INNER JOIN product_parameter PP
                    ON PP.parameter_id=PARAM.id
                    WHERE PARAM.name='{param_name}'
                    GROUP BY PP.value
                ) _
            )
            
            ORDER BY durability DESC
            LIMIT {limit}
            OFFSET {offset * limit}
        ''')

        result = cursor.fetchall()
        full_count = 0
        if len(result) > 0:
            full_count = result[0][-1]
        cols = Product.columns() + ["durability", "c_rank"] + [":category"] + Category.columns() + [":manufacturer"] + Manufacturer.columns()
        products = Model.serializeJoin(cols, result)

        return full_count, products