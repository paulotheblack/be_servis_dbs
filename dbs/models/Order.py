from dbs.models.Category import Category
from dbs.models.Manufacturer import Manufacturer
from dbs.models.Model import Model
from dbs.models.Product import Product


class Order(Model):
    table = "orders"
    _columns = ["id",
                "date",
                "price",
                "status",
                "first_name",
                "last_name",
                "email",
                "phone_number",
                "address",
                "city",
                "zip",
                "country",
                "uuid"
                ]

    @classmethod
    def find_by_uuid(cls, uuid, cursor):
        cols = [
            "id",
            "date",
            "price",
            "status",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "address",
            "city",
            "zip",
            "country",
            "uuid"
        ]
        cursor.execute(f'''
            SELECT {",".join(cols)}
            FROM orders
            WHERE uuid='{uuid}'
        ''')
        _item = cursor.fetchone()

        # if found, serialize it
        if _item:
            item = cls.serialize(_item, cols)
            return item
        return None

    @classmethod
    def products(cls, uuid, cursor):
        product_cols = Product.columns()
        category_cols = Category.columns()
        manufacturer_cols = Manufacturer.columns()

        cols = ["quantity"] + product_cols + [":manufacturer"] + manufacturer_cols + [":category"] + category_cols

        cursor.execute(f'''
            SELECT
                op.quantity,
                P.{",P.".join(product_cols)},
                ':manufacturer', M.{",M.".join(manufacturer_cols)},
                ':category', C.{",C.".join(category_cols)}
            FROM order_product op

            INNER JOIN products P
            ON P.id=op.product_id
            
            INNER JOIN orders O
            ON op.order_id=O.id
            
            INNER JOIN manufacturers M
            ON P.manufacturer_id=M.id
            
            INNER JOIN categories C
            ON P.category_id=C.id
            
            WHERE O.uuid='{uuid}'
        ''')

        result = cursor.fetchall()
        items = Model.serializeJoin(cols, result)
        return items

    @classmethod
    def search(cls, cursor, s, limit=5, offset=0):
        splitted = s.split(" ")
        both_names_str = ""
        if len(splitted) == 2:
            both_names_str = f"OR (first_name='{splitted[0]}' AND last_name='{splitted[1]}')"
        cursor.execute(f'''
                SELECT
                    {",".join(cls._columns)},
                    COUNT(*) OVER() AS full_count
                FROM {cls.table}
                WHERE (uuid::text = '{s}')
                OR (LOWER(first_name) LIKE '%{s.split(" ")[0]}%')
                OR (LOWER(last_name) LIKE '%{s}%')
                OR (LOWER(email) LIKE '%{s}%')
                OR (LOWER(address) LIKE '%{s}%')
                {both_names_str if len(splitted) == 2 else ''}
                OR (LOWER(city) LIKE '%{s}%')
                OR (LOWER(zip) LIKE '%{s}%')
                ORDER BY id
                LIMIT {limit}
                OFFSET {limit * offset}
            ''')
        _items = cursor.fetchall()
        items = [cls.serialize(item[:-1], cls._columns) for item in _items]
        if len(items) > 0:
            full_count = _items[0][-1]
            return full_count, items
        else:
            return 0, []
