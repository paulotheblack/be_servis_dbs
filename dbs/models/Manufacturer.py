from dbs.models.Model import Model


class Manufacturer(Model):
    table = "manufacturers"
    _columns = ["id", "name", "img_urls"]

    @classmethod
    def products(cls, id, cursor, limit=10):
        cols = ["id", "name", "img_urls", "category_id", "price", "description"]
        cursor.execute(f'''
            SELECT {",".join(cols)}
            FROM products
            WHERE manufacturer_id={id}
            LIMIT {limit}
        ''')
        products = cursor.fetchall()
        _products = [cls.serialize(product, cols) for product in products]
        return _products

    @classmethod
    def most_popular_products(cls, id, cursor, limit=10):
        cols = ["id", "name", "img_urls", "category_id", "price", "description"]
        cursor.execute(f'''
            SELECT {",".join(cols)}
            FROM products
            WHERE manufacturer_id={id}
            ORDER BY visited DESC
            LIMIT {limit}
        ''')
        products = cursor.fetchall()
        _products = [cls.serialize(product, cols) for product in products]
        return _products

    @classmethod
    def products_count(cls, id, cursor):
        cursor.execute(f'''
                SELECT COUNT(id)
                FROM products
                WHERE manufacturer_id={id}
            ''')
        count = cursor.fetchone()[0]
        return count