from dbs.models.Model import Model


class Category(Model):
    table = "categories"
    _columns = ["id", "name", "description"]